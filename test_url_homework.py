import requests
import allure


@allure.feature("宠物商店宠物信息接口测试")
class TestPetstore:
    def setup_class(self):
        # 基础的数据信息
        pass

    @allure.story("查询宠物接口冒烟用例")
    def test_getpet(self):

        data = {
            'key1': 1,
            'key2': 2
        }
        get_url = 'https://petstore.swagger.io/#/pet/findPetsByStatus'
        #r = requests.get("https://petstore.swagger.io/#/pet/findPetsByStatus/get", params=data, proxies=proxys, verify=False)
        with allure.step("发出查询接口请求"):
            self.find_params = data
            r = requests.get(get_url, self.find_params)
        with allure.step("获取查询接口响应"):
            print(r.text)
        with allure.step("查询接口断言"):
            assert r.status_code == 200

    @allure.story("新增宠物接口冒烟用例")
    def test_postpet(self):
        proxys = {
            'http': '127.0.0.1:8888',
            'https': '127.0.0.1:8888'
        }
        post_url = 'https://petstore.swagger.io/v2/pet'
        data = '{"id": "cat", "category": {"id": "cat","name": "mimi"},"name": "doggie","photoUrls": ["s"],"tags": [{"id": "cat","name": "str"}],"status": "available"}'
        r = requests.post(post_url, json=data, proxies=proxys, verify=False)
        print(r.json())
        assert r.status_code == 500









