import pytest
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

class TestWebHomework:
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    def test_web(self):
        self.driver.get("https://ceshiren.com")
        self.driver.find_element(By.LINK_TEXT, "精华帖").click()
        sleep(2)
        self.driver.find_element(By.LINK_TEXT, "思寒漫谈测试人职业发展").click()
        sleep(2)
        self.driver.back()
        sleep(2)
        assert self.driver.find_element(By.LINK_TEXT, "思寒漫谈测试人职业发展").text == "思寒漫谈测试人职业发展"


    if __name__ == '__main__':
        test_web()