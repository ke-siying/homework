import pytest

def setup_function():
    print("开始计算")
def teardown_function():
    print("结束计算")
def teardown():
    print("结束测试")

def add(x, y):
    return x + y

@pytest.mark.hebeu
def test_hebeu1():
    assert add(2, 3) == 5
    assert add(2.31, 3.4) == 5.71
    assert add(3, 2.1) == 5.1

@pytest.mark.hebeu
def test_hebeu2():
    assert add(-99, 99) == 0
    assert add(-99, 0) == -99
    assert add(99, 0) == 99


